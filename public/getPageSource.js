var interval = window.setInterval(function() {
  console.log('fetched page source', Date.now());
  chrome.runtime.sendMessage({
    action: 'getSource',
    source: document.body.innerText
    // source: DOMtoString(document)
  });
}, 1000);

// http://stackoverflow.com/questions/11684454/getting-the-source-html-of-the-current-page-from-chrome-extension
// function DOMtoString(document) {
//   var html = '';
//   var node = document.firstChild;
//
//   while (node) {
//     switch (node.nodeType) {
//       case Node.ELEMENT_NODE:
//         html += node.outerHTML;
//         break;
//       case Node.TEXT_NODE:
//         html += node.nodeValue;
//         break;
//       case Node.CDATA_SECTION_NODE:
//         html += '<![CDATA[' + node.nodeValue + ']]>';
//         break;
//       case Node.COMMENT_NODE:
//         html += '<!--' + node.nodeValue + '-->';
//         break;
//       case Node.DOCUMENT_TYPE_NODE:
//         // (X)HTML documents are identified by public identifiers
//         html += "<!DOCTYPE " + node.name + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '') + (!node.publicId && node.systemId ? ' SYSTEM' : '') + (node.systemId ? ' "' + node.systemId + '"' : '') + '>\n';
//         break;
//     }
//     node = node.nextSibling;
//   }
//   return html;
// }

// var port = chrome.runtime.connect({ name: "Chang3" });
//   port.onDisconnect.addListener(function() {
//     console.log("Disconnected");
//   });
