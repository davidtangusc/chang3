export default [
  {
    type: 'Tornado'
  },
  {
    type: 'Cyclone'
  },
  {
    type: 'Flood'
  },
  {
    type: 'Fire'
  },
  {
    type: 'Typhoon'
  }
];
