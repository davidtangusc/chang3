import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    verifyPaymentDetails() {
      this.transitionToRoute('verify-payment-details');
    }
  }
});
