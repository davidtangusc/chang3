import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    donate() {
      this.transitionToRoute('successful-payment');
    }
  }
});
