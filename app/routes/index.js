import Ember from 'ember';

let { chrome } = window;
let { Route, computed } = Ember;

export default Route.extend({
  inChromeExtension: computed(function() {
    return Boolean(chrome.runtime.onMessage);
  }),
  setupController(controller) {
    if (this.get('inChromeExtension')) {
      onPageExtractInterval((request) => {
        controller.set('model', {
          pageSource: request.source
        });
      }, (lastError) => {
        controller.set('model', {
          error: `There was an error injecting script: ${lastError.message}`
        });
      });
    } else {
      // stub out page scraping
    }
  }
});

function onPageExtractInterval(success, error) {
  chrome.runtime.onMessage.addListener(function(request/*, sender*/) {
    if (request.action === 'getSource') {
      success(request);
    }
  });

  // null means active tab
  chrome.tabs.executeScript(null, { file: 'getPageSource.js' }, function() {
    // If you try and inject into an extensions page or the webstore/NTP you'll get an error
    if (chrome.runtime.lastError) {
      error(chrome.runtime.lastError);
    }
  });
}
