# Chang3

## Workflow via Chrome Extension

1. Open this article in Chrome: http://www.express.co.uk/news/world/766658/Avalanche-France-ski-resort-Lake-Tignes-death-total
2. Open the Chrome extension

## Useful Links

* https://developer.chrome.com/extensions/getstarted
* http://stackoverflow.com/questions/31058879/how-to-configure-ember-routes-in-a-chrome-extension
* http://stackoverflow.com/questions/11684454/getting-the-source-html-of-the-current-page-from-chrome-extension


Hey David, so the excel sheet requires three things

1. A list of words that could indicate a disaster (indicators), i.e. tragedy, dead, wounded, tragic, etc
2. A list of geographical places (areas), i.e. Town, city, country names
3. A list of disaster types (types): i.e. terrorist attack, cyclone, tsunami, etc

Using these ingredients, there are 3 steps.

1. You scan the article and see if there are more than 3 'indicators'.
2. If there are less than 3 indicators then this article is marked 'not valid' and nothing else happens. If there are more than 3 indicators then the formula proceeds, when scanning the article, the number of times that each of the 'areas' and 'types' occurs, the system selects the area and type that occurs the most often.
3. The article is scanned again and used to populate the sentence "Would you like to make a chang3 for those in, (areas) affected by the (types)" to populate this, the area and the type that occur the most often in the article are selected.
